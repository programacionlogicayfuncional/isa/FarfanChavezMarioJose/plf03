(ns plf03.core)

;;-----------------comp--------------------------------
(defn comp-1 []
  (let [f (fn [y] (inc y))
        g (fn [y] (inc y))
        h (fn [y] (/ 1 y))
        
        z (comp h f g)]
    (z 10)))


(defn prueba [x] (let [f (fn [y] (inc y))
        g (fn [y] (inc y))
        h (fn [y] (/ 1 y))]
 (h (f (g x))) ))
(prueba 10)

(defn comp-2 []
  (let [f (fn [y] (inc y))
        g (fn [y] (dec y))
        h (fn [y] (/ 1 y))

        z (comp h f g)]
    (z 10)))

(defn comp-3 []
  (let [f (fn [x] expresion)
        g (fn [y] expresion)
        h (fn [x y] expresion)

        z (A f g h)]
    (z argumentos)))

(complement-1 10)
;;-----------------complement--------------------------
(defn complement-1 [x]
  ((complement even?) x))

(defn complement-2 [x] 
  ((complement odd?) x))

(defn complement-3 [x]
  ((complement boolean?) x))

(defn complement-4 [x] 
  ((complement integer?) x))

(defn complement-5 [x] 
  ((complement string?) x))

(defn complement-6 [x]
  ((complement some?) x))

(defn complement-7 [x]
  ((complement any?) x))

(defn complement-8 [x]
  ((complement empty?) x))

(defn complement-9 [x]
  ((complement zero?) x))

(defn complement-10 [x] 
  ((complement pos?) x))

(defn complement-11 [x]
 ((complement neg?) x))

(defn complement-12 [x]
  ((complement neg-int?) x))

(defn complement-13 [x] 
  ((complement pos-int?) x))

(defn complement-14 [x] 
  ((complement rational?) x))

(defn complement-15 [x] 
  ((complement ratio?) x))

(defn complement-16 [x] 
  ((complement decimal?) x))

(defn complement-17 [x] 
  ((complement float?) x))

(defn complement-18 [x] 
  ((complement double?) x))

(defn complement-19 [x] 
  ((complement nat-int?) x))

(defn complement-20 [x] 
  ((complement char?) x))

(complement-1 10)
(complement-2 10)
(complement-3 true)
(complement-4 11)
(complement-5 "hola")
(complement-6 [1 2 3])
(complement-6 "hola")
(complement-8 [1 2 3])
(complement-9 1)
(complement-10 1)
(complement-11 -1)
(complement-12 12)
(complement-12 -12)
(complement-14 1)
(complement-15 3)
(complement-16 1.100001)
(complement-17 1.1)
(complement-18 2.22)
(complement-19 20)
(complement-20 \q)
;;-----------------constantly--------------------------
(defn constantly-1 []
  (constantly 10))

(defn constantly-2 [] 
  (constantly 20)))

(defn constantly-3 [] 
  (constantly 30))

(defn constantly-4 []
  (constantly 40))

(defn constantly-5 []
  (constantly-5 12))

(defn constantly-6 []
  (constantly 60))

(defn constantly-7 [] 
  (constantly 70))

(defn constantly-8 []
  (constantly 80))

(defn constantly-9 [] 
  (constantly 90))

(defn constantly-10 []
  (constantly 100))

(defn constantly-11 []
  (constantly 11))

(defn constantly-12 []
  (constantly 12))

(defn constantly-13 []
  (constantly 12))

(defn constantly-14 []
  (constantly 14))

(defn constantly-15 []
  (constantly 15))

(defn constantly-16 []
  (constantly 16))

(defn constantly-17 []
  (constantly 17))

(defn constantly-18 []
  (constantly 18))

(defn constantly-19 []
  (constantly 19))

(defn constantly-20 []
  (constantly 20))

(constantly-1)
(constantly-2)
(constantly-3)
(constantly-4)
(constantly-6)
(constantly-7)
(constantly-8)
(constantly-9)
(constantly-10)
(constantly-11)
(constantly-12)
(constantly-13)
(constantly-14)
(constantly-15)
(constantly-16)
(constantly-17)
(constantly-18)
(constantly-19)
(constantly-20)

;;-----------------every-pred--------------------------
(defn every-pred-1 [x y]
  ((every-pred number? x y)))

(defn every-pred-2 [x]
  ((every-pred number? pos?) x))

(defn every-pred-3 [x]
  ((every-pred number? int? pos?) x))

(defn every-pred-4 [x]
  ((every-pred number? float? pos?) x))

(defn every-pred-5 [x]
  ((every-pred number? zero? every?) x))

(defn every-pred-6 [x]
  ((every-pred number? rational? ratio?) x))

(defn every-pred-7 [x]
  ((every-pred some? string?) x))

(defn every-pred-8 [x] 
  ((every-pred some? number? float?) x))

(defn every-pred-9 [x]
  ((every-pred float? neg?) x))

(defn every-pred-10 [x]
  ((every-pred neg? rational?) x))

(defn every-pred-11 [x] 
  ((every-pred rational? pos?) x))

(defn every-pred-12 [x]
  ((every-pred int? neg?) x))

(defn every-pred-13 [x]
  ((every-pred float? pos?) x))

(defn every-pred-14 [x]
  ((every-pred int? even?) x))

(defn every-pred-15 [x]
  ((every-pred int? odd?) x))

(defn every-pred-16 [x]
  ((every-pred int? even? pos?) x))

(defn every-pred-17 [x]
  ((every-pred int? odd? pos?) x))

(defn every-pred-18 [x] 
  ((every-pred int? even? neg?) x))

(defn every-pred-19 [x]
  ((every-pred int? odd? neg?) x))

(defn every-pred-20 [x]
  ((every-pred some? string?) x))

(every-pred-1 1 "we")
(every-pred-2 {2 3 4 -1})
(every-pred-3 -1)
(every-pred-4 1.1)
(every-pred-5 1)
(every-pred-6 2)
(every-pred-7 1)
(every-pred-8 9)
(every-pred-9 1)
(every-pred-10 0)
(every-pred-11 1)
(every-pred-12 -1)
(every-pred-13 -1)
(every-pred-14 1.1)
(every-pred-15 1)
(every-pred-16 2)
(every-pred-17 1)
(every-pred-18 9)
(every-pred-19 1)
(every-pred-20 {2 3 4 -1})
;;-----------------fnil--------------------------------
(defn fnil-1 []
  (( ( fnil (str "hola ")) "mario")))

(defn fnil-2 [] 
  (fnil (+ 2) 4))

(defn fnil-3 []
  (fnil (* 2) 4))

(defn fnil-4 []
  (fnil (/ 2) 4))

(fnil-1)
(fnil-2)
(fnil-3)
(fnil-4)

;;-----------------juxt--------------------------------

;;-----------------partial-----------------------------

;;-----------------some-fn-----------------------------

